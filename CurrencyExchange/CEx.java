import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * 
 */

/**
 * @author Tim
 * Date: 12/3/2017, rev: 1
 * 
 * Currency Exchange Input Module (GUI)
 */
public class CEx extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JComboBox<String> jcbFromCurrency = new JComboBox<String>();
	private JComboBox<String> jcbToCurrency = new JComboBox<String>();
	private JTextField jtfFromAmount = new JTextField();
	private JTextField jtfToAmount = new JTextField();

	private CEx() {
		super("Currency Exchange");
		setLayout(new GridLayout(3, 3, 10, 10));
		
		jcbFromCurrency.addItem("Pesos");
		jcbFromCurrency.addItem("US Dollar");
		jcbToCurrency.addItem("Pesos");
		jcbToCurrency.addItem("US Dollar");
		
		add(jtfFromAmount);
		add(jcbFromCurrency);
		add(new JLabel(">> CONVERT >>"));
		add(jtfToAmount);
		add(jcbToCurrency);
		add(new JTextField("Message Area"));

	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CEx currencyExchange = new CEx();
		currencyExchange.pack();
		currencyExchange.setSize(1600, 800);
		currencyExchange.setVisible(true);
		currencyExchange.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}


}
